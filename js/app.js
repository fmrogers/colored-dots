var ColorDots = function(dots, rows, addNewDots) {
    // Properties
    this.numOfDots = dots;
    this.numOfRows = rows || 1;
    this.addNewDots = addNewDots || false;
    
    this.frame = document.getElementById('frame');    

    // Initialise Methods
    this.buildDots();
    this.renderRows();
    this.addNewDot();   
    this.newRandomColor();
  
};

/**
 * TYPE: Method
 * NAME: buildDots
 * PARAMETERS: none
 * DESCRIPTION: This method returns a html string with three randomly 
 * generated numbers ranging between 0 and 255. Using the method 
 * 'randomColor()' the random numerical values are stored in three 
 * seperate variables representing the RGB value code, Red, Green, Blue. 
 * These values are then used concatinated to the inline CSS style color.
 */

ColorDots.prototype.buildDots = function() {
    var r = this.randomColor();
    var g = this.randomColor();
    var b = this.randomColor();

    return '<i class="fa fa-circle color-dot dot" aria-hidden="true" style="color:rgb(' + r + ', ' + g + ', ' + b + ')"></i>'; 
};

/**
 * TYPE: Method
 * NAME: renderRows
 * PARAMETERS: none
 * DESCRIPTION: This method renders the necessary html required to build
 * build each row of dots. The rows are built individually in a loop that 
 * repeats a number of times equal to the numeric value specified in 
 * 'this.numOfRows'. Each row is then populated with dots in a similar 
 * fashion using a loop that repeats equal to the numeric value of 
 * 'this.numOfDots'. The dots are built using the method 'this.buildDots()'.
 */

ColorDots.prototype.renderRows = function() {    
    
    var rowHtml = ''; 
     
    for ( var r = 0; r < this.numOfRows; r++ ) {
                
        rowHtml += '<div class="color-dot-row row">';

        for ( var i = 0; i < this.numOfDots; i++ ) {

            rowHtml += this.buildDots();

        }  
        
        if ( this.addNewDots === true ) {

            rowHtml += '<span class="fa fa-plus-circle add-new-dot dot" aria-hidden="true"></span>';

        }

        rowHtml += '</div>';
    }

    this.frame.innerHTML = rowHtml;
    
};

/**
 * TYPE: Method
 * NAME: randomColor
 * PARAMETER: none
 * DESCRIPTION: This method returns a random numeric value between 0 and 255. 
 * The value corresponds to a indevidual RBG color value.
 */

ColorDots.prototype.randomColor = function() {

    return Math.floor(Math.random() * 256);

};

/**
 * TYPE: Method
 * NAME: addNewDot
 * PARAMETER: none
 * DESCRIPTION: This method loops over each row using a html collection and adds
 * a new HTML element after the last dot of each row. This element is in assigned
 * a click event that in turn adds a new dot to the markup after the last dot of 
 * each row. Each new dot built using the method 'this.buildDots()'.
 */

ColorDots.prototype.addNewDot = function() {

    var _this = this; 

    var newDots = document.getElementsByClassName('add-new-dot');      

    var addDot = function(target) {

        var newDot = _this.buildDots();
        
        target.insertAdjacentHTML('beforebegin', newDot);

        _this.newRandomColor();

    };
    
    this.frame.addEventListener('click', function(event) {

        if (event.target.tagName == 'SPAN') {

            addDot(event.target);

        }

    }, false);  

};

/**
 * TYPE: Method
 * NAME: newRandomColor
 * PARAMETER: none
 * DESCRIPTION: This method binds a clicke event to the indevidual dots and uses a 
 * local function to change the inline style of the dots. The inline style channge
 * makes use of the 'this.randomColor()'.
 */

ColorDots.prototype.newRandomColor = function() {

    var _this = this;

    var dots = document.getElementsByClassName('color-dot');    

    var changeColor = function(target) {
        
       target.style.color = 'rgb(' + _this.randomColor() + ', ' + _this.randomColor() + ', ' + _this.randomColor() + ')';

    };
    
    this.frame.addEventListener('click', function(event) {
        
        if (event.target.tagName == 'I') {
            
            changeColor(event.target);
        }        

    }, false);


};

/**
 * TESTING AREA 
*/ 
var test = new ColorDots(5, 5, true);
console.log(test);
